# Cas d'usage de visio-conférence

## Vocabulaire

* Maître de Cérémonie (MC) : la personne qui décide de l'ouverture de micros, caméra, et éventuellement modère tchat

## Périmètre des cas d'usages

Cas d'usage                                       | Interactions                                               | Personnes
--------------------------------------------------|------------------------------------------------------------|-----------
Réunion de travail                                | N-N entre tous les participants                            | <10
Assemblée Générale, webinaire, formation en ligne | Asymétriques : <br> scène => public <br> scène <- public   | scène <10 ; public <1000
Concert, webcast                                  | Descendantes : <br> scène => public <br> public <-> public | scène <10 ; public : jusqu'à limite technique

## Configuration micro / caméra / modération

Cas d'usage                                       |  Maître Cérémonie | Micro                                                  | Caméra                 
--------------------------------------------------|-------------------|--------------------------------------------------------|------------------------
Réunion de travail                                |  N.A.             | :green_heart: ouvert                                   | :green_heart: ouverte 
Assemblée Générale, webinaire, formation en ligne |  Requis           | :large_orange_diamond: modéré par MC, fermé par défaut | :large_orange_diamond:  modéré par MC
Concert, webcast                                  |  N.A.             | :red_circle: fermé                                     | :red_circle: fermé

