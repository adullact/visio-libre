#  Revue de presse

## 2020 Décembre

* 2020-12-23 [Zoom Shared US User Data With Beijing](https://www.ntd.com/zoom-shared-us-user-data-with-beijing_544087.html)

## 2020 Novembre

* 2020-11-10 ArsTechnica [Zoom lied to users about end-to-end encryption for years, FTC says](https://arstechnica.com/tech-policy/2020/11/zoom-lied-to-users-about-end-to-end-encryption-for-years-ftc-says/), où on découvre **aussi** que le binaire de Zoom a volontairement contourné des mesures de sécurité de MacOSX mises en place par Apple (!)
  

## 2020 octobre

* :warning: 2020-10-26 **NextInpact [Zoom censure un évènement qui devait évoquer sa censure](https://www.nextinpact.com/lebrief/44353/zoom-censure-evenement-qui-devait-evoquer-sa-censure)**

## Août 2020

* 2020-08-18 NextInpact [Chiffrement : l’ONG Consumer Watchdog s’attaque aux fausses allégations de Zoom](https://www.nextinpact.com/article/43379/chiffrement-long-consumer-watchdog-sattaque-aux-fausses-allegations-zoom)

## Juillet 2020

* 2020-07-15 NextInpact [Faille critique pour Zoom sur Windows 7 et versions antérieures](https://www.nextinpact.com/brief/faille-critique-pour-zoom-sur-windows-7-et-versions-anterieures-13085.htm)
* 2020-07-07, 01Net [La plupart des services de visioconférence ne sont pas conformes au RGPD](https://www.01net.com/actualites/la-plupart-des-services-de-visioconference-ne-sont-pas-conformes-au-rgpd-1943911.html).
  > Microsoft Teams, Google Meet, Skype ou Zoom ont récolté un « feu rouge »

## Juin 2020

* 2020-06-18, NextInpact [Finalement, Zoom proposera bien du chiffrement de bout en bout (E2E) à tous ses clients](https://www.nextinpact.com/brief/finalement--zoom-proposera-bien-du-chiffrement-de-bout-en-bout--e2e--a-tous-ses-clients-12782.htm) (mais en donnant son numéro de téléphone)

## Mai 2020

* :warning: 2020-05-04, **Usine Digitale [Visioconférence : Webex, Microsoft Teams et Google Meet critiqués pour leur gestion des données personnelles](https://www.usine-digitale.fr/article/visioconference-webex-microsoft-teams-et-google-meet-critiques-pour-leur-gestion-des-donnees-personnelles.N960611)**
  > [les solutions de visio] ont toutes des politiques de confidentialité très permissives, qui autorisent notamment l'utilisation de ces vidéos pour entraîner des **systèmes de reconnaissance faciale**. 

## Avril 2020

* 2020-04-30, Consumer Report [It's Not Just Zoom. Google Meet, Microsoft Teams, and Webex Have Privacy Issues, Too.](https://www.consumerreports.org/video-conferencing-services/videoconferencing-privacy-issues-google-microsoft-webex/)
* 2020-04-23, NextInpact [Zoom 5.0 va apporter les améliorations de sécurité promises, mais la route reste longue](https://www.nextinpact.com/brief/zoom-5-0-va-apporter-les-ameliorations-de-securite-promises--mais-la-route-reste-longue-12127.htm)
* 2020-04-22, NextInpact [Selon d’anciens employés, Dropbox savait pour la sécurité cataclysmique de Zoom](https://www.nextinpact.com/brief/selon-d-anciens-employes--dropbox-savait-pour-la-securite-cataclysmique-de-zoom-12116.htm)
* 2020-04-09, CNIL [COVID-19 : les conseils de la CNIL pour utiliser les outils de visioconférence](https://www.cnil.fr/fr/covid-19-les-conseils-de-la-cnil-pour-utiliser-les-outils-de-visioconference)
* :warning: 2020-04-08, **NextInpact [La Direction interministérielle du numérique « déconseille fortement » l'application Zoom](https://www.nextinpact.com/news/108877-la-direction-interministerielle-numerique-deconseille-fortement-application-zoom.htm)**
* 2020-04-03, NextInpact [Zoom : retour sur une accumulation de failles](https://www.nextinpact.com/news/108854-zoom-retour-sur-accumulation-failles.htm)
* 2020-04-02, The Guardian [‘Zoom is malware’: why experts worry about the video conferencing platform](https://www.theguardian.com/technology/2020/apr/02/zoom-technology-security-coronavirus-video-conferencing)

## Mars 2020

* 2020-03-31, The Intercept [Zoom Meetings Aren’t End-to-End Encrypted, Despite Misleading Marketing](https://theintercept.com/2020/03/31/zoom-meeting-encryption/)
* 2020-03-27, Motherboard (Vice.com) [Zoom Removes Code That Sends Data to Facebook](https://www.vice.com/en_us/article/z3b745/zoom-removes-code-that-sends-data-to-facebook)
* :warning: 2020-03-27, **Clubic [Sur iOS, ZOOM partage vos données avec Facebook (même si vous n'avez pas de compte)](https://www.clubic.com/os-mobile/iphone-os/actualite-889935-ios-appli-zoom-partage-donnees-facebook-avez-compte.html)**
* 2020-03-26 Motherboard (Vice.com) [Zoom iOS App Sends Data to Facebook Even if You Don’t Have a Facebook Account](https://www.vice.com/en_ca/article/k7e599/zoom-ios-app-sends-data-to-facebook-even-if-you-dont-have-a-facebook-account)
