# Limites techniques des outils 

## Microsoft Teams (remplaçant de Skype Business)

* Réunions
    * Nombre de personnes dans une réunion : 250
    * Nombre de personnes dans un appel vidéo ou audio de la conversation : 20
* Événements en direct Teams
    * Taille de l’audience : 10000 participants
    * Durée de l’événement : 4 heures
* Navigateurs : Firefox non supporté (!)

Sources : [Limites et spécifications de Microsoft Teams](https://docs.microsoft.com/fr-fr/microsoftteams/limits-specifications-teams)

## Zoom

* [Zoom system & bandwith requirements](https://support.zoom.us/hc/en-us/articles/201362023-System-Requirements-for-PC-Mac-and-Linux#h_d278c327-e03d-4896-b19a-96a8f3c0c69c)

## Webex

TODO


